var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/Userdb');
var User = mongoose.model('User', mongoose.Schema({// notre modéle
	name:String,
	siret: Number,
	adresse: String,
	contact:String,
	nb_employe: Number
}));
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client'));

app.get('/api/Userdb', function(req, res){
	User.find(function(err, users){
		if(err)
			res.send(err);
		res.json(users);
	});
});

app.get('/api/Userdb/:id', function(req, res){
	User.findOne({_id:req.params.id}, function(err, user){
		if(err)
			res.send(err);
		res.json(user);
	});
});
app.post('/api/Userdb', function(req, res){
	User.create( req.body, function(err,users){
		if(err)
			res.send(err);
		res.json(users);
	});
});

app.delete('/api/Userdb/:id', function(req, res){
	User.findOneAndRemove({_id:req.params.id}, function(err, user){
		if(err)
			res.send(err);
		res.json(user);
	});
});
app.put('/api/Userdb/:id', function(req, res){
	var query = {
		name:req.body.name,
		siret:req.body.siret,
		adresse:req.body.adresse,
		contact:req.body.contact,
		nb_employe :req.body.nb_employe
	};
	User.findOneAndUpdate({_id:req.params.id}, query, function(err, user){
		if(err)
			res.send(err);
		res.json(user);
	});
});
app.listen(3000, function(){
	console.log('server is running on port 3000..');
});
