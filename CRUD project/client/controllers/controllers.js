myApp.controller('empController', function($scope,$route,$routeParams,$http){
	$scope.getUsers = function(){
		$http.get('/api/Userdb/').then(function(response){
			$scope.users = response.data;
		});
	};
	$scope.showUser = function(){
		var id = $routeParams.id;
		$http.get('/api/Userdb/'+ id).then(function(response){
			$scope.user = response.data;
		});
	};
	$scope.addUser = function(){
		$http.post('/api/Userdb/', $scope.user).then(function(response){
			window.location.href = '/';
		});
	};
	$scope.updateUser = function(){
		var id = $routeParams.id;
		$http.put('/api/Userdb/'+ id , $scope.user).then(function(response){
			window.location.href = '/';
		});
	};
	$scope.deleteUser = function(id){
		var id = id;
		$http.delete('/api/Userdb/'+ id).then(function(response){
			$route.reload();
		});
	};

});
