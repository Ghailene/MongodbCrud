var myApp = angular.module('myApp',['ngRoute']);
myApp.config(function($routeProvider){
	$routeProvider
		.when('/', {
			templateUrl:'templates/list.html',
			controller:'empController'
		})
		.when('/Userdb', {
			templateUrl:'templates/list.html',
			controller:'empController'
		})
		.when('/Userdb/create', {
			templateUrl:'templates/add.html',
			controller:'empController'
		})
		.when('/Userdb/:id/edit', {
			templateUrl:'templates/edit.html',
			controller:'empController'
		})
		.when('/Userdb/:id/show', {
			templateUrl:'templates/show.html',
			controller:'empController'
		});
});
